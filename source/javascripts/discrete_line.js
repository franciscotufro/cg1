// DiscreteLineBresenham = function(x0, y0, x1, y1, canvas) {
//   var context = canvas.getContext('2d');
//   var pixels = context.createImageData(canvas.width, canvas.height);
//     
//   var x = x0;
//   var y = y0;
//   var dx = x1 - x0;
//   var dy = y1 - y0;
// 
//   var symmetries = { A: null, B: null, C: (Math.abs(dx) < Math.abs(dy)) };
//   symmetries.A = symmetries.C ? dx < 0 : dy < 0;
//   symmetries.B = symmetries.C ? dy < 0 : dx < 0;
//   
//   // if(symmetries.C){
//   //   x0, y0 = swap(x0, y0);
//   //   x1, y1 = swap(x1, y1);
//   // }
//   
//   var ix = 2 * dx;
//   var iy = 2 * dy;
//   e = iy - dx;
// 
//   
//   for ( x; x <= x1; x++) {
//     paintPixel(x, y, canvas, pixels);
//     if (e > 0){
//       y++;
//       e = e - ix;
//     }
//     e = e + iy;
//   }
//   
//   context.putImageData(pixels, 0, 0);
//   
// }
// 
// DiscreteLineNative = function(x0, y0, x1, y1, context) {
//   
// }
// 
// function paintPixel(x,y,canvas, pixels){
//   // Uso round acá para no ensuciar el código de arriba
//   x = Math.round(x);
//   y = Math.round(y);
//   
//   var p = (y * canvas.width * 4) + (x * 4);
//   pixels.data[p] = 255;
//   pixels.data[p+1] = 255;
//   pixels.data[p+2] = 255;
//   pixels.data[p+3] = 255;
// }
// 