Tp2 = new Item('#tp2');

Tp2.engine = RenderEngine;
Tp2.engine.framebuffer = Tp2.framebuffer;

Tp2.leaves = 0;

clearInterval(Tp2.interval);
this.objects = [];

Tp2.drawing = false;

Tp2.draw = function (){
  if(!Tp2.drawing){
    Tp2.drawing = true;
    Tp2.framebuffer.clear();
    RenderEngine.render(Tp2, null);
    Tp2.framebuffer.push();
    Tp2.drawing = false;
  }
};

Tp2.addObject = function (object){
  this.objects.push(object);
};

Tp2.update = function(){
  // this.objects = [];

  var leaves = Number($($(Tp2.name + '_hojas')[0]).val());

  if(leaves != Tp2.leaves){
    Tp2.objects = [];
    Tp2.leaves = leaves;
    Tp2.setupScene ();
  }

  // Movamos las hojas
  $.each(Tp2.objects, function(i, tree){
    $.each(tree.objects, function(i, leaf){
      if(! leaf.draw){
        // Muevo las hojas al (0,0)
        var m = leaf.transform;
        var t_x = m.elements[0][2];
        var t_y = m.elements[1][2];

        Translate(leaf, -t_x, -t_y)
        
        // Deshago la rotación anterior
        // if (leaf.previousRotation) {
        //   Rotate(leaf, - leaf.previousRotation);
        // }
        
        var rotation = Random(5)
        Rotate(leaf, rotation)
        //leaf.previousRotation = rotation
        Translate(leaf, t_x, t_y); 
     }
    });
  });


};

Tp2.setupScene = function (){
  var tree = new Tree (Tp2.leaves)
  Scale(tree, 0.3, 0.3 );
  Translate(tree, 40, 40);
  Tp2.addObject(tree)

  tree = new Tree (Tp2.leaves)
  Scale(tree, 0.5, 0.5 );
  Translate(tree, 300, 90);
  Tp2.addObject(tree)

  // tree = new Tree (Tp2.leaves)
  // Scale(tree, 0.6, 0.6 );
  // Rotate(tree, -45);
  // Translate(tree, 200, 100);
  // Tp2.addObject(tree)

  tree = new Tree (Tp2.leaves)
  Translate(tree, 50, 200);
  Tp2.addObject(tree)

  // tree = new Tree (Tp2.leaves)
  // Scale(tree, 1.4, 1.4 );
  // Translate(tree, 200, 250);
  // Tp2.addObject(tree)
}

// Defino una hoja
Leaf = function (){
  var leaf = {}
  var point0 = new Point(0, 0, new IntegerColor(0, 135, 0, 1));
  var point1 = new Point(20, 0, new IntegerColor(121, 135, 0, 1));
  var point2 = new Point(0, 20, new IntegerColor(100, 120, 114, 1));

  leaf.objects = [new Triangle(point0, point1, point2)];

  return leaf;
}

Tree = function (leaves){
  var tree = {}
  var point0 = new Point(0, 300, new IntegerColor(140, 94, 53, 1));
  var point1 = new Point(100, 300, new IntegerColor(191, 132, 75, 1));
  var point2 = new Point(50, 0, new IntegerColor(116, 61, 6, 1));

  tree.objects = [];
  
  tree.objects.push(new Triangle(point0, point1, point2));

  for(var i = 0; i < leaves; i++){
    var leaf = new Leaf ();

    Rotate(leaf, Random(90));
    Translate(leaf, 50 + Random(80), Random(80));

    tree.objects.push( leaf );
  }
  tree.setFramebuffer = function(framebuffer){
    $.each(self.objects, function(i, object){ object.setFramebuffer(framebuffer)});
  }
  tree.draw = function(){
    $.each(self.objects, function(i, object){ object.draw();});
  }

  return tree;
}

// Devuelve entre un numero entero entre 0 y top
function Random(top) {
  return Math.floor(Math.random() * 2 * top) - top;
}

// Devuelve entre un numero real 0 y top
function RandomFloat(min, max) {
  return (Math.random() * max) + min;
}

//Tp2.update();
setInterval(Tp2.update, 1000.0/60.0)
Tp2.interval = setInterval(Tp2.draw, 1000.0 / 60.0);
