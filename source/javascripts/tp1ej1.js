// Ejercicio 1a
Tp1Ej1 = new Item('#tp1ej1');
var point1 = new Point(Number($($(Tp1Ej1.name + '_x0')[0]).val()), Number($($(Tp1Ej1.name + '_y0')[0]).val()), Red);
var point2 = new Point(Number($($(Tp1Ej1.name + '_x1')[0]).val()), Number($($(Tp1Ej1.name + '_y1')[0]).val()), Green);
var segment = new Segment(point1, point2);

Tp1Ej1.update = function(){
  Tp1Ej1.objects[0].x0.x = Number($($(Tp1Ej1.name + '_x0')[0]).val());
  Tp1Ej1.objects[0].x0.y = Number($($(Tp1Ej1.name + '_y0')[0]).val());
  Tp1Ej1.objects[0].x1.x = Number($($(Tp1Ej1.name + '_x1')[0]).val());
  Tp1Ej1.objects[0].x1.y = Number($($(Tp1Ej1.name + '_y1')[0]).val());
}

setInterval(Tp1Ej1.update, 1000.0/60.0)

Tp1Ej1.addSegment(segment);


// Ejercicio 1b
Tp1Ej1b = new Item('#tp1ej1b');
var point1 = new Point(Number($($(Tp1Ej1b.name + '_x0')[0]).val()), Number($($(Tp1Ej1b.name + '_y0')[0]).val()), Red);
var point2 = new Point(Number($($(Tp1Ej1b.name + '_x1')[0]).val()), Number($($(Tp1Ej1b.name + '_y1')[0]).val()), Green);
var segment = new Segment(point1, point2);
segment.algorithm = 'bressenham';

Tp1Ej1b.update = function(){
  Tp1Ej1b.objects[0].x0.x = Number($($(Tp1Ej1b.name + '_x0')[0]).val());
  Tp1Ej1b.objects[0].x0.y = Number($($(Tp1Ej1b.name + '_y0')[0]).val());
  Tp1Ej1b.objects[0].x1.x = Number($($(Tp1Ej1b.name + '_x1')[0]).val());
  Tp1Ej1b.objects[0].x1.y = Number($($(Tp1Ej1b.name + '_y1')[0]).val());
}

setInterval(Tp1Ej1b.update, 1000.0 / 60.0)

Tp1Ej1b.addSegment(segment);

