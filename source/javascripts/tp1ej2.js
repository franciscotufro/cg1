Tp1Ej2 = new Item('#tp1ej2');

Tp1Ej2.update = function(){
  Tp1Ej2.objects = [];

  var x0 = {
    x: Number($($(Tp1Ej2.name + '_x0')[0]).val()),
    y: Number($($(Tp1Ej2.name + '_y0')[0]).val()),
    r: Number($($(Tp1Ej2.name + '_r0')[0]).val()),
    g: Number($($(Tp1Ej2.name + '_g0')[0]).val()),
    b: Number($($(Tp1Ej2.name + '_b0')[0]).val()),
  };

  var x1 = {
    x: Number($($(Tp1Ej2.name + '_x1')[0]).val()),
    y: Number($($(Tp1Ej2.name + '_y1')[0]).val()),
    r: Number($($(Tp1Ej2.name + '_r1')[0]).val()),
    g: Number($($(Tp1Ej2.name + '_g1')[0]).val()),
    b: Number($($(Tp1Ej2.name + '_b1')[0]).val()),
  };

  var x2 = {
    x: Number($($(Tp1Ej2.name + '_x2')[0]).val()),
    y: Number($($(Tp1Ej2.name + '_y2')[0]).val()),
    r: Number($($(Tp1Ej2.name + '_r2')[0]).val()),
    g: Number($($(Tp1Ej2.name + '_g2')[0]).val()),
    b: Number($($(Tp1Ej2.name + '_b2')[0]).val()),
  };

  var point0 = new Point(x0.x, x0.y, new Color(x0.r, x0.g, x0.b, 1));
  var point1 = new Point(x1.x, x1.y, new Color(x1.r, x1.g, x1.b, 1));
  var point2 = new Point(x2.x, x2.y, new Color(x2.r, x2.g, x2.b, 1));
  var triangle = new Triangle(point0, point1, point2);
  
  Tp1Ej2.addTriangle(triangle);
}

//Tp1Ej2.update();
//Tp1Ej2.draw();
setInterval(Tp1Ej2.update, 1000.0/60.0)