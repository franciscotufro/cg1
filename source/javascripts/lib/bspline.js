BSpline = function (control) {
  this.framebuffer = null;
  this.control = control;

  this.addControlPoint = function (point) {
    point.setFramebuffer(this.framebuffer);
    this.control.push(point);
  };

  this.setFramebuffer = function(framebuffer){
    this.framebuffer = framebuffer;
  };
  
  this.draw = function () {
    if ( this.framebuffer ){
      $.each(this.control, function(i, point){
        point.draw();
      });

      this.drawCurve();  

    }else{
      console.log("Missing framebuffer")
    }
  };
  
  this.drawCurve = function () {
    var me = this;

    for(var i = 0; i < this.control.length; i++){
      var p0 = this.control[i-1];
      var p1 = this.control[i];
      var p2 = this.control[i+1];
      var p3 = this.control[i+2];
      
      if(i == 0){ p0 = p1; }

      if(i == this.control.length - 2){ p3 = p2; }
      if(i == this.control.length - 1){ p2 = p1; p3 = p1; }

      this.drawCurveSegment(p0, p1, p2, p3);
    }
  };

  // En lugar de calcular la base utilicé la matriz asociada.
  this.drawCurveSegment = function (p0, p1, p2, p3){

    for(var i = 0; i <= 1; i += 0.001){

      var i_vector = $M([[Math.pow(i,3), Math.pow(i,2), i, 1]]);
      var spline_matrix = $M([
        [-1, 3,-3, 1],
        [ 3,-6, 3, 0],
        [-3, 0, 3, 0],
        [ 1, 4, 1, 0]
        ]);
      var p_matrix = $M([
        [p0.x, p0.y], 
        [p1.x, p1.y], 
        [p2.x, p2.y], 
        [p3.x, p3.y]]);
      
      var mat = spline_matrix.x(p_matrix);
      var pos = i_vector.x( mat ).multiply(1/6);
      
      var x = pos.elements[0][0];
      var y = pos.elements[0][1];
      this.framebuffer.putPixel(x, y, Red);
    }
  };


}