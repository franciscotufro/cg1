Framebuffer = function (canvas) {
  this.canvas = canvas;
  this.context = canvas.getContext('2d');
  this.pixels = this.context.createImageData(canvas.width, canvas.height);

  this.clear = function (){
    this.pixels = this.context.createImageData(canvas.width, canvas.height);
  };
  
  this.width = function(){
    return this.canvas.width;
  };
  
  this.height = function(){
    return this.canvas.height;
  }
  
  
  this.putPixel = function (x, y, color){
    // Avoid drawing a pixel out of bounds
    if (x < 0 || y < 0 || x >= canvas.width || y >= canvas.height)
      return;

    x = Math.round(x);
    y = Math.round(y);
    

    var p = (y * canvas.width * 4) + (x * 4);
    this.pixels.data[p] = color.r;
    this.pixels.data[p+1] = color.g;
    this.pixels.data[p+2] = color.b;
    this.pixels.data[p+3] = color.a;
  };
  
  this.push = function (){
    this.context.putImageData(this.pixels, 0, 0);
  }
  
}