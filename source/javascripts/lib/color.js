// Color receives r, g, b and a ranging from 0 to 1
Color = function (r, g, b, a) {
  this.r = 255 * r;
  this.g = 255 * g;
  this.b = 255 * b;
  this.a = 255 * a;
}

IntegerColor = function (r, g, b, a) {
  this.r = Math.ceil(r);
  this.g = Math.ceil(g);
  this.b = Math.ceil(b);
  this.a = Math.ceil(a);
}

White = new Color(1,1,1,1);
Red = new Color(1,0,0,1);
Green = new Color(0,1,0,1);
Blue = new Color(0,0,1,1);
