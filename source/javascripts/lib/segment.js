Segment = function (x0, x1) {
  this.x0 = x0;
  this.x1 = x1;
  this.color = White;
  this.algorithm = 'dda';

  this.setFramebuffer = function(framebuffer) {
    this.framebuffer = framebuffer;
    this.x0.setFramebuffer(framebuffer);
    this.x1.setFramebuffer(framebuffer);
  };
  
  this.draw = function () {
    this.x0.draw();
    this.x1.draw();
    switch(this.algorithm){
      case 'dda':
        this.drawDDA();
        break;
      case 'bressenham':
        this.drawBressenham();
        break;
      case 'native':
        this.drawNative();
        break;
    }
  };
  
  this.drawDDA = function (){
    var pixels = this.framebuffer.pixels;
    var x0 = this.x0.x;
    var y0 = this.x0.y;

    var x1 = this.x1.x;
    var y1 = this.x1.y;
    
    var dx = x1 - x0;
    var dy = y1 - y0;
  
    var symmetries = { A: null, B: null, C: (Math.abs(dx) < Math.abs(dy)) };
    symmetries.A = symmetries.C ? dx < 0 : dy < 0;
    symmetries.B = symmetries.C ? dy < 0 : dx < 0;
    
    var iterator = symmetries.C ? y0 : x0;
    var iteratorEnd = symmetries.C ? y1 : x1;
    var dependent = symmetries.C ? x0 : y0;
  
    var m = symmetries.C ? dx / dy : dy / dx;
    
    // Si tenemos que hacer simetria A entonces nos aseguramos
    // de que la pendiente es negativa, y si no tenemos que hacerla
    // nos aseguramos que la pendiente sea positiva
    m = symmetries.A && m > 0 ? -m : m;
    m = !symmetries.A && m < 0 ? -m : m;
    
    var iterate = false
    
    do {
      this.framebuffer.putPixel( (symmetries.C ? dependent : iterator), (symmetries.C ? iterator : dependent), this.color );
      dependent = dependent + m;
      symmetries.B ? iterator-- : iterator++;
      iterate = symmetries.B ? iterator >= iteratorEnd : iterator <= iteratorEnd
    }while ( iterate )
    
  };
  
  this.drawBressenham = function (){
    var x0 = this.x0.x;
    var y0 = this.x0.y;
    
    var x1 = this.x1.x;
    var y1 = this.x1.y;
    
    var dx = x1 - x0;
    var dy = y1 - y0;
    var swappedAxis = (Math.abs(dy) > Math.abs(dx));
    
    if( swappedAxis ){
      var tmp = x0; x0 = y0; y0 = tmp;
      tmp = x1; x1 = y1; y1 = tmp;
      dx = x1 - x0;
      dy = y1 - y0;
    }

    if (dx < 0) {
      var tmp = x0; x0 = x1; x1 = tmp;
      tmp = y0; y0 = y1; y1 = tmp;
      dx = x1 - x0;
      dy = y1 - y0;
    }
    var positiveSlope = (dy > 0 && dx < 0) || (dy < 0 && dx > 0);
    
    var ix = 2*dx;
    var iy = 2*dy;
    
    var e = positiveSlope ? - dx - iy : dx - iy;
    var y = y0;
    
    for(var x = x0; x <= x1; x++){
      this.framebuffer.putPixel( swappedAxis ? y : x, swappedAxis ? x : y, this.color );

      if ( (positiveSlope && e > 0) || (!positiveSlope && e < 0) ) {
        positiveSlope ? y-- : y++;
        e = positiveSlope ? e - ix - iy : e + ix - iy;
      }else{
        e = e - iy
      }
    }
  };
    
}