// Cada ejercicio va a tener su propio framebuffer dentro del canvas
Item = function (name) {
  var me = this;
  this.name = name;
  
  // Set canvas size
  this.canvas = $(name + '_canvas')[0];
  this.canvas.width = $(this.canvas).width();
  this.canvas.height = $(this.canvas).height();
  
  this.framebuffer = new Framebuffer(this.canvas);
  this.objects = [];
  
  this.addPoint = function(point) {
    point.setFramebuffer(this.framebuffer);
    this.objects.push(point);
  };
  
  this.addSegment = function(segment) {
    segment.setFramebuffer(this.framebuffer);
    this.objects.push(segment);
    
  };
  
  this.addTriangle = function(triangle){
    triangle.setFramebuffer(this.framebuffer);
    this.objects.push(triangle);
  };

  this.addObject = function(object){
    object.setFramebuffer(this.framebuffer);
    this.objects.push(object);
  }
  
  this.draw = function () {
    me.framebuffer.clear ();
    $.each(me.objects, function(i, object){
      object.draw();
    })
    me.framebuffer.push();
  }
  
  this.update = function(){
    console.log("Update!");
  }
  this.interval = setInterval(this.draw, 1000.0 / 60.0);
}