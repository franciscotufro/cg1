Bezier = function (control) {
  this.framebuffer = null;
  this.control = control;

  this.addControlPoint = function (point) {
    point.setFramebuffer(this.framebuffer);

    var size = this.control.length;

    if(this.control[size-1] && this.control[size-1].length % 4 > 0){
      this.control[size-1].push(point);
    }else{
      this.control.push([point]);
    }
  };

  this.setFramebuffer = function(framebuffer){
    this.framebuffer = framebuffer;
  };
  
  this.draw = function () {
    if ( this.framebuffer ){
      $.each(this.control, function(i, points){
        $.each(points, function(i, point){
          point.draw();
        });
      });

      this.drawCurve();  

    }else{
      console.log("Missing framebuffer")
    }
  };
  
  this.drawCurve = function () {
    var me = this;
    $.each(this.control, function(i, points){

      if(points.length % 4 == 0){
        var p0 = points[0];
        var p1 = points[1];
        var p2 = points[2];
        var p3 = points[3];

        me.drawCurveSegment(p0, p1, p2, p3);  
      }
    });
  };

  this.drawCurveSegment = function (p0, p1, p2, p3){
    for(i = 0; i <= 1; i += 0.001){
      var b_x = Math.pow(1-i, 3) * p0.x + 3*Math.pow(1-i, 2)*i * p1.x + 3*(1-i)*Math.pow(i, 2) * p2.x + Math.pow(i, 3) * p3.x;
      var b_y = Math.pow(1-i, 3) * p0.y + 3*Math.pow(1-i, 2)*i * p1.y + 3*(1-i)*Math.pow(i, 2) * p2.y + Math.pow(i, 3) * p3.y;
      this.framebuffer.putPixel(b_x, b_y, Green);
    }
  };


}