RenderEngine = {
  objects: [],
  
  framebuffer: null,

  render: function(graph, currentTransform){
    var me = this;

    if(!currentTransform){
      currentTransform = Matrix.I(3);
    } else {
      if(graph.transform){
        currentTransform = currentTransform.x(graph.transform);
      }
    }
    graph.currentTransform = currentTransform;
    
    if(graph.objects instanceof Array){
      $.each(graph.objects, function(i, node) {
        me.render(node, currentTransform);
      });
    }else{
      graph.setFramebuffer(me.framebuffer);
      graph.draw();
    }
    graph.currentTransform = null
  },
}


function Scale(o, sx, sy){
  var m = $M([[sx, 0, 0], [0, sy, 0], [0, 0, 1]]);
  applyTransform(o, m);
}

function Rotate(o, r){
  // Pasamos r de grados a radianes
  r = (r * Math.PI) / 180;
  var m = $M([[Math.cos(r), -Math.sin(r), 0], [Math.sin(r), Math.cos(r), 0], [0, 0, 1]]);
  applyTransform(o, m);
}

function Translate(o, tx, ty){
  var m = $M(([[1,0,tx], [0,1,ty], [0, 0, 1]]));
  applyTransform(o, m);
}

function applyTransform(o, m){
  if(o.transform){
    o.transform = m.x(o.transform);  
  }else{
    o.transform = m;
  }
}