Point = function (x,y,color) {
  this.x = x;
  this.y = y;
  this.color = color;
  this.framebuffer = null;
  
  this.setFramebuffer = function(framebuffer){
    this.framebuffer = framebuffer;
  };
  
  this.draw = function () {
    if ( this.framebuffer ){
      for( i = -1; i < 2; i++){
        for ( j = -1; j < 2; j++){
          this.framebuffer.putPixel(this.x + i, this.y + j, this.color);
        }
      }
    }else{
      console.log("Missing framebuffer")
    }
  }
  
}