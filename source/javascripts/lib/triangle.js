Triangle = function (x0, x1, x2) {
  this.x0 = x0;
  this.x1 = x1;
  this.x2 = x2;

  this.maxx = [];
  this.minx = [];

  this.maxr = [];
  this.minr = [];
  this.maxg = [];
  this.ming = [];
  this.maxb = [];
  this.minb = [];
  
  this.transform = null;

  this.setFramebuffer = function(framebuffer) {
    this.framebuffer = framebuffer;
    this.x0.setFramebuffer(framebuffer);
    this.x1.setFramebuffer(framebuffer);
    this.x2.setFramebuffer(framebuffer);
    
    for(var i = 0; i <= framebuffer.height(); i++){
      this.maxx[i] = -1;
      this.minx[i] = framebuffer.height() + 1;

      this.maxr[i] = -1;
      this.minr[i] = 1;

      this.maxg[i] = -1;
      this.ming[i] = 1;

      this.maxb[i] = -1;
      this.minb[i] = 1;
    }
  };
  
  this.draw = function () {
    var final_x0 = new Point(this.x0.x, this.x0.y, this.x0.color);
    var final_x1 = new Point(this.x1.x, this.x1.y, this.x1.color);
    var final_x2 = new Point(this.x2.x, this.x2.y, this.x2.color);

    if(this.currentTransform){
      // Transformo x0, x1 y x2 en vectores columna para
      // operar con matrices
      var t_x0 = $M([[this.x0.x], [this.x0.y], [1]]);
      var t_x1 = $M([[this.x1.x], [this.x1.y], [1]]);
      var t_x2 = $M([[this.x2.x], [this.x2.y], [1]]);

      final_x0.x = Math.floor(this.currentTransform.x(t_x0).elements[0][0]);
      final_x0.y = Math.floor(this.currentTransform.x(t_x0).elements[1][0]);
      final_x1.x = Math.floor(this.currentTransform.x(t_x1).elements[0][0]);
      final_x1.y = Math.floor(this.currentTransform.x(t_x1).elements[1][0]);
      final_x2.x = Math.floor(this.currentTransform.x(t_x2).elements[0][0]);
      final_x2.y = Math.floor(this.currentTransform.x(t_x2).elements[1][0]);

    }


    this.bucketBressenham(final_x0,final_x1);
    this.bucketBressenham(final_x1,final_x2);
    this.bucketBressenham(final_x2,final_x0);
    this.drawBucket();
  };
  
  this.bucketBressenham = function (point1, point2){
    var x0 = point1.x;
    var y0 = point1.y;
    
    var x1 = point2.x;
    var y1 = point2.y;
    
    var dx = x1 - x0;
    var dy = y1 - y0;

    var swappedAxis = (Math.abs(dy) > Math.abs(dx));
    if( swappedAxis ){
      var tmp = x0; x0 = y0; y0 = tmp;
      tmp = x1; x1 = y1; y1 = tmp;
      dx = x1 - x0;
      dy = y1 - y0;
    }

    
    var swappedLimits = dx < 0;
    if (swappedLimits) {
      var tmp = x0; x0 = x1; x1 = tmp;
      tmp = y0; y0 = y1; y1 = tmp;
      dx = x1 - x0;
      dy = y1 - y0;
    }
    
    var positiveSlope = (dy > 0 && dx < 0) || (dy < 0 && dx > 0);
    
    var ix = 2*dx;
    var iy = 2*dy;
    
    var e = positiveSlope ? - dx - iy : dx - iy;
    var y = y0;
    
    // Para hacer la interpolación de color seleccionamos los bordes
    // Si dx < 0 entonces intercambiamos los puntos
    var color0 = (swappedLimits) ? point2.color : point1.color;
    var color1 = (swappedLimits) ? point1.color : point2.color;
    
    var colorStep = { 
      r: (color1.r - color0.r) / Math.abs(x1 - x0), 
      g: (color1.g - color0.g) / Math.abs(x1 - x0), 
      b: (color1.b - color0.b) / Math.abs(x1 - x0)
    }

    var r = color0.r;
    var g = color0.g;
    var b = color0.b;

    for(var x = x0; x <= x1; x++){
      if(swappedAxis) {
        if (y > this.maxx[x]) { 
          this.maxx[x] = y 
          this.maxr[x] = r;
          this.maxg[x] = g;
          this.maxb[x] = b;
        };

        if (y < this.minx[x]) {
          this.minx[x] = y 
          this.minr[x] = r;
          this.ming[x] = g;
          this.minb[x] = b;
          
        };
      }else{
        if (x > this.maxx[y]) { 
          this.maxx[y] = x 
          this.maxr[y] = r;
          this.maxg[y] = g;
          this.maxb[y] = b;
        };
        
        if (x < this.minx[y]) { 
          this.minx[y] = x 
          this.minr[y] = r;
          this.ming[y] = g;
          this.minb[y] = b;
        };
      }

      if ( (positiveSlope && e > 0) || (!positiveSlope && e < 0) ) {
        positiveSlope ? y-- : y++;
        e = positiveSlope ? e - ix - iy : e + ix - iy;
      }else{
        e = e - iy
      }
      r += colorStep.r;
      g += colorStep.g;
      b += colorStep.b;
    }
  };
  
  this.drawBucket = function(){
    for(var y = 0; y < this.framebuffer.height(); y++){
      // Chequeo que para este valor de y tenga que dibujar algo
      if(this.maxx[y] > -1 && this.minx[y] < (this.framebuffer.height() + 1)){
        var deltaX = this.maxx[y] - this.minx[y];
          
        var deltaR = (this.maxr[y] - this.minr[y]) / deltaX;
        var deltaG = (this.maxg[y] - this.ming[y]) / deltaX;
        var deltaB = (this.maxb[y] - this.minb[y]) / deltaX;

        var r = this.minr[y];
        var g = this.ming[y];
        var b = this.minb[y];

        for(var x = this.minx[y]; x <= this.maxx[y]; x++){
          this.framebuffer.putPixel( x, y, new IntegerColor(r,g,b,255) );
          r += deltaR;
          g += deltaG;
          b += deltaB;
        }
      }
    }
  };
  
  
}