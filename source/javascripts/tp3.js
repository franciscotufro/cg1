Tp3 = function (id, curve){
  var tp = new Item(id);

  //clearInterval(tp.interval);
  tp.curve = curve;
  tp.addObject(curve);

  $(tp.canvas).click(function(e){
    if(tp.curve){
      var x = e.offsetX;
      var y = e.offsetY;

      var point = new Point(x, y, White)
      tp.curve.addControlPoint(point);
    }
  });
}

var bz = new Bezier([]);
Tp3Bezier = new Tp3('#tp3_bezier', bz);

var bs = new BSpline([]);
Tp3BSplines = new Tp3('#tp3_bsplines', bs);


